#!/usr/bin/env python

"""
Make combined CSV of sequences from Figure S2.
"""

import re
import sys
from csv import DictWriter
from collections import defaultdict

def seqdict(path):
    seqs = defaultdict(str)
    seqid = None
    with open(path) as f_in:
        for line in f_in:
            line = line.strip()
            if line.startswith(">"):
                seqid = line[1:]
            else:
                seqs[seqid] += line
    return seqs

def combine_s2(paths_in, path_out):
    fig_map = {
        "S2A": "HeavyNT",
        "S2B": "HeavyAA",
        "S2C": "LightNT",
        "S2D": "LightAA"}
    entries = defaultdict(dict)
    for path in paths_in:
        match = re.search("(S2[ABCD]).*.fa$", path)
        col = fig_map[match.group(1)]
        for seqid, seq in seqdict(path).items():
            cat = "RUA" if "RUA" in seqid else "mAb"
            entries[seqid]["Entry"] = seqid
            entries[seqid]["Category"] = cat
            entries[seqid][col] = seq
    entries = list(entries.values())
    entries.sort(key=lambda x: x["Entry"])
    with open(path_out, "w") as f_out:
        writer = DictWriter(
            f_out,
            ["Entry", "Category", "HeavyNT", "HeavyAA", "LightNT", "LightAA"],
            lineterminator="\n")
        writer.writeheader()
        writer.writerows(entries)

if __name__ == "__main__":
    combine_s2(sys.argv[1:-1], sys.argv[-1])
