#!/usr/bin/env python

"""
Make combined CSV of sequences from GenBank entries.
"""

import re
import sys
from csv import DictWriter
from Bio import SeqIO

# JQ267519.1 Homo sapiens isolate CH01 anti-HIV1 immunoglobulin light chain variable region mRNA, partial cds
def combine_genbank(paths_in, path_out):
    rows = []
    for path in paths_in:
        for rec in SeqIO.parse(path, "fasta"):
            match = re.match("([A-Z0-9.]+) Homo sapiens isolate (CH[0-9]+) anti-HIV1 immunoglobulin (heavy|light) chain variable region mRNA, partial cds", rec.description)
            rows.append({
                "Accession": match.group(1),
                "Antibody": match.group(2),
                "Chain": match.group(3),
                "Seq": str(rec.seq)})
    rows = sorted(rows, key=lambda r: (r["Antibody"], r["Chain"]))
    with open(path_out, "w") as f_out:
        writer = DictWriter(
            f_out, fieldnames=["Antibody", "Chain", "Accession", "Seq"], lineterminator="\n")
        writer.writeheader()
        writer.writerows(rows)

if __name__ == "__main__":
    combine_genbank(sys.argv[1:-1], sys.argv[-1])
