#!/usr/bin/env python

"""
Configure Figure S2 panel text into FASTA.
"""

import re
import sys
from collections import defaultdict

def glom_chunk(entries, chunk):
    for key in chunk:
        entries[key] += chunk[key]
    chunk.clear()

def proc_seq_line(chunk, line):
    match = re.search("([^ ]+) +(.*)", line)
    seqid, seq = match.groups()
    seq = re.sub(" ", "", seq)
    chunk[seqid] = seq

def parse_s2(path, path_out):
    entries = defaultdict(str)
    chunk = {}
    newchunk = False
    with open(path) as f_in:
        for line in f_in:
            if line == "\n":
                # skip blank lines
                continue
            if line.startswith(" "):
                # we've hit a new chunk; process the old one
                glom_chunk(entries, chunk)
                continue
            proc_seq_line(chunk, line)
        # process the last chunk too
        glom_chunk(entries, chunk)
    with open(path_out, "w") as f_out:
        for key, val in entries.items():
            f_out.write(f">{key}\n{val}\n")

if __name__ == "__main__":
    parse_s2(sys.argv[1], sys.argv[2])
