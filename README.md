# Data Gathering from Bonsignori 2011

Some sequences and metadata from:

Mattia Bonsignori et al.
Analysis of a Clonal Lineage of HIV-1 Envelope V2/V3 Conformational Epitope-Specific Broadly Neutralizing Antibodies and Their Inferred Unmutated Common Ancestors.
Journal of Virology, Volume 85, No 19, 998 – 10009
<https://doi.org/10.1128/jvi.05045-11>

 * Human Subject CH0219
 * Antibodies CH01 - CH04

GenBank entries (not linked via the paper) for each antibody's heavy and light
chain variable region sequences:

 * CH01: JQ267523 JQ267519
 * CH02: JQ267524 JQ267520
 * CH03: JQ267525 JQ267521
 * CH04: JQ267526 JQ267522

Parsed info here:

 * `output/seqs_figS2.csv`: full-length antibody and RUA sequences from figure
   S2, parsed and reformatted
 * `output/seqs_genbank.csv`: antibody variable region sequences from GenBank
