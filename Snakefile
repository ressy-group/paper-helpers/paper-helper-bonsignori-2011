with open("genbank_accessions.txt") as f_in:
    GB_ACCESSIONS = [line.strip() for line in f_in]

TARGET_GBF_FASTA = expand("from-genbank/{acc}.fasta", acc=GB_ACCESSIONS)

rule all:
    input: expand("output/seqs_{thing}.csv", thing=["figS2", "genbank"])

rule combine_genbank:
    output: "output/seqs_genbank.csv"
    input: TARGET_GBF_FASTA
    shell: "python scripts/combine_genbank.py {input} {output}"

rule all_gbf_fasta:
    input: TARGET_GBF_FASTA

rule download_gbf_fa:
    """Download one FASTA file per GenBank accession."""
    output: "from-genbank/{acc}.fasta"
    shell: "python scripts/download_ncbi.py nucleotide {wildcards.acc} > {output}"

rule combine_s2:
    output: "output/seqs_figS2.csv"
    input: expand("parsed/S2{panel}_full_ungapped.fa", panel=["A", "B", "C", "D"])
    shell: "python scripts/combine_s2.py {input} {output}"

rule ungapped_s2:
    output: "parsed/{panel}_full_ungapped.fa"
    input: "parsed/{panel}_full.fa"
    shell:
        """
            awk '!/^>/ {{gsub(/-/,"")}} {{ print $$0}}' < {input} > {output}
        """

rule explicit_s2:
    output: "parsed/{panel}_full.fa"
    input: "parsed/{panel}.fa"
    shell: "python scripts/explicit_s2.py {input} {output}"

rule parse_s2:
    output: "parsed/{panel}.fa"
    input: "from-paper/fig{panel}.txt"
    shell: "python scripts/parse_s2.py {input} {output}"
